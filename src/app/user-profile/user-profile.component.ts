import {Component, OnInit} from '@angular/core';
import {DataService} from '../services/data.service';
import {Subject} from 'rxjs';
import Swal from 'sweetalert2'
import {EmailValidator} from '@angular/forms';
// import * as EmailValidator from 'email-validator';

declare var $: any;


@Component({
    selector: 'app-user-profile',
    templateUrl: './user-profile.component.html',
    styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

    reslt;
    validateEmail = false;

    persons = [];
    spinner = false;
    dtTrigger: Subject<any> = new Subject();
    model: any = {
        name: '',
        email: '',
        password: '',
        c_password: ''
    };
    user_id: any;
    invalideEmail: any;
    invalidePwd: any;
    is_matched: any;

    constructor(private dataService: DataService) {
        //     pagingType: 'full_numbers',
        //     // pageLength: 2
        // };

        this.fetchUser();

    }


    ngOnDestroy(): void {
        this.dtTrigger.unsubscribe();
    }

    // get all users
    fetchUser() {
        // this.dataService.getUsers().subscribe((persons) => {
        //     this.persons = persons;
        //     console.log(persons);
        //     this.spinner = true;
        //     this.dtTrigger.next();
        // })
        this.dataService.getUsers().subscribe(result => {
            this.persons = result;
        })
    }

    // clear all text fiald
    clearTextField() {
        this.model.name = '';
        this.model.email = '';
        this.model.password  = '';
        this.model.c_password = '';

    }

    // alert
    showNotification(from, align, mtype, message) {
        const type = [mtype];

        const color = Math.floor((Math.random() * 4) + 1);

        $.notify({
            icon: 'notifications',
            message: '<b style="color: black">' + message + '<b>'

        }, {
            type: type[color],
            timer: 3500,
            placement: {
                from: from,
                align: align
            },
        });
    }

    // add new user to main db
    addNewRecord() {
        let temp = this.checkPassword();
        if (temp){
            if (!this.validateEmails()) {
                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Wrong Email Combanation..!',
                    footer: '<a href>Somthinthin wrong with email</a>'
                })
            } else {

                this.dataService.addNewUser(this.model).subscribe((result) => {
                    if (result.success) {
                        this.dtTrigger.unsubscribe();
                        this.clearTextField();
                        this.fetchUser();
                        this.showNotification('bottom', 'right', 'save', 'User Save Successed');
                    } else {
                        // this.showNotification('bottom', 'right', 'danger', 'Check Your Email');
                        Swal.fire({
                            type: 'error',
                            title: 'Oops...',
                            text: 'Your Email Already Exist..!',
                            footer: '<a href>Somthinthin wrong with email</a>'
                        })
                    }
                });
                this.clearTextField();
            }
        }else {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Password Conformation Incorrect..!',
                footer: '<a href>Plase Check Your Password </a>'
            })
        }
    }

    // check Password
    checkPassword(){
        return this.model.c_password == this.model.password;
    }


    // email validation
    validateEmails() {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return  re.test(String(this.model.email).toLowerCase());
    }

    // password validation
    validatePwd() {
        var re = /^([a-zA-Z0-9]{8,20})$/;
        this.invalidePwd = re.test(String(this.model.password).toLowerCase());
    }

    // password confirmation validate
    matchPwd() {
        if (this.model.password !== this.model.c_password) {
            this.is_matched = false
        } else {
            this.is_matched = true;
        }
    }

    // delete user from database
    deleteUser(id) {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    this.dataService.deleteUser(id).subscribe((result) => {
                        this.reslt = result;
                        if (this.reslt.message != null) {
                            this.fetchUser();
                            this.showNotification('bottom', 'right', 'delete', this.reslt.message);
                        } else if (this.reslt.success) {
                            this.fetchUser();
                            this.showNotification('bottom', 'right', 'delete', 'User Delete Successed');
                        } else {
                            this.showNotification('bottom', 'right', 'danger', 'User Not Delete');
                        }

                    })
                }
        })
    }

    ngOnInit(): void {
    }



}
