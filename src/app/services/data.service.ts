import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {map, catchError, tap} from 'rxjs/operators';
import {getResponseURL} from '@angular/http/src/http_utils';

const endpoint = 'https://cytmainapi.herokuapp.com/api/';
// const endpoint = 'http://localhost:5050/api/';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};

@Injectable({
    providedIn: 'root'
})
export class DataService {

    constructor(private http: HttpClient) {
    }

    private extractData(res: Response) {
        let body = res;
        return body || {};
    }

    public login = false;

    // get all users
    getUsers(): Observable<any> {
        return this.http.get(endpoint + 'users/all').pipe(
            map(this.extractData));
    }

    // get all unique devices
    getUniqueDevices(filter): Observable<any> {
        let val = {
            'offset': filter.offset,
            'limit': 10,
            'sortBy': 'slugName',
            'sort': filter.sort
        };
        return this.http.post(endpoint + 'device/getalluniquedevice', val, httpOptions).pipe(
            map(this.extractData)
        );
    }

    // get all websites device details
    getProducts(filter): Observable<any> {
        let val = {
            'offset': filter.offset,
            'limit': 10,
            'sortBy': 'capacity',
            'sort': filter.sort
        };

        return this.http.post<any>(endpoint + 'device/getall', val, httpOptions).pipe(
            map(this.extractData)
        );
        // return this.http.get(endpoint + 'device/getall').pipe(
        //   map(this.extractData));
    }


    // get all scrapping site logs
    getScrappingSitesLog(siteName): Observable<any> {
        return this.http.post<any>(endpoint + 'device/getlogs', {siteName: siteName}, httpOptions).pipe(
            map(this.extractData)
        );
    }
    // get all distinct modleName
    getAllModleName(): Observable<any> {
        return this.http.post<any>(endpoint + 'device/getall_distinct_data', {filter: "modleName"}, httpOptions).pipe(
            map(this.extractData)
        );
    }
    // get all distinct siteNames
    getAllSites(): Observable<any> {
        return this.http.post<any>(endpoint + 'device/getall_distinct_data', {filter: "siteName"}, httpOptions).pipe(
            map(this.extractData)
        );
    }
    // get all distinct networkName
    getAllNetwork(): Observable<any> {
        return this.http.post<any>(endpoint + 'device/getall_distinct_data', {filter: "networkName"}, httpOptions).pipe(
            map(this.extractData)
        );
    }
    // get all distinct brandName
    getAllBrands(): Observable<any> {
        return this.http.post<any>(endpoint + 'device/getall_distinct_data', {filter: "brandName"}, httpOptions).pipe(
            map(this.extractData)
        );
    }
    // get all distinct capacity
    getAllCapacity(): Observable<any> {
        return this.http.post<any>(endpoint + 'device/getall_distinct_data', {filter: "capacity"}, httpOptions).pipe(
            map(this.extractData)
        );
    }
    // get all distinct conditions
    getAllConditons(): Observable<any> {
        return this.http.post<any>(endpoint + 'device/getall_distinct_data', {filter: "condition"}, httpOptions).pipe(
            map(this.extractData)
        );
    }


    // get distinct scrapping sites list
    getScrappedSites(): Observable<any> {
        return this.http.get(endpoint + 'device/getScrappedSites', httpOptions).pipe(
            map(this.extractData)
        );
    }

    // filter main scrapping data
    filterScrappingData(filter, query): Observable<any> {

        // console.log(filter.sort);

        let val = {
            'offset': filter ? filter.offset : 0,
            'limit': 10,
            'sortBy': 'capacity',
            'sort': filter ? filter.sort : -1,
            'siteName': query.siteName,
            'brandName': query.brandName,
            'modleName': query.modelName,
            'networkName': query.networkName,
            'capacity': query.capacity,
            'condition': query.condition,
            'price_from': query.priceFrom,
            'price_to': query.priceTo

        };
        return this.http.post<any>(endpoint + 'device/filter', val, httpOptions).pipe(
            map(this.extractData)
        );
    }



    // user login
    loginUser(user): Observable<any> {
        console.log(user);
        return this.http.post<any>(endpoint + 'login/' + user.email + '/' + user.password, httpOptions).pipe(
            tap((response) => console.log('login successful')),
            catchError(this.handleError<any>('logIn'))
        );
    }

    // add unique device to main db
    addDevice(deviceName): Observable<any> {
        return this.http.post<any>(endpoint + 'device/adduniquedevice', deviceName, httpOptions).pipe(
            tap((response) => console.log('Device added', response)),
            catchError(this.handleError<any>('adduniquedevice'))
        );

    }

    // add new user to main db
    addNewUser(user): Observable<any> {
        var t = localStorage.getItem('token');
        let options = { // Header
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + t,
                'Accept': 'application/json',
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': 'Content-Type, Authorization',
            })
        };

        // console.log(user);
        return this.http.post<any>(endpoint + 'users/setup', user, options).pipe(
            tap((response) => console.log('add new user successful')),
            catchError(this.handleError<any>('logIn'))
        );
    }

    // Update user password
    changePassword(user): Observable<any> {
        // console.log(user);

        var token = localStorage.getItem('token');
        let options = { // Header
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
                'Accept': 'application/json',
                'Access-Control-Allow-Methods': 'PUT',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': 'Content-Type, Authorization',
            })
        };

        console.log(options);
        return this.http.put<any>(endpoint + 'users/update/', user, options).pipe(
            tap((response) => console.log('password change successful')),
            catchError(this.handleError<any>('update'))
        );

    }

    // delete user from database
    deleteUser(user_id): Observable<any> {

        var t = localStorage.getItem('token');
        let options = { // Header
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + t,
                'Accept': 'application/json',
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': 'Content-Type, Authorization',
            })
        };
        return this.http.delete(endpoint + 'users/delete/' + user_id, options).pipe(
            tap((response) => this.getUsers()),
            catchError(this.handleError<any>('delete'))
        );
    }

    filterData(filterObject): Observable<any> {
        var t = localStorage.getItem('token');
        let options = { // Header
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + t,
                'Accept': 'application/json',
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': 'Content-Type, Authorization',
            })
        };

        console.log(filterObject);
        return this.http.post<any>(endpoint + 'device/filter', filterObject, httpOptions).pipe(
            tap((response) => console.log('response for filter - ', response)),
            catchError(this.handleError<any>('filter'))
        );
    }


    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            console.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }


}
