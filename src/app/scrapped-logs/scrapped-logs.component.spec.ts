import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScrappedLogsComponent } from './scrapped-logs.component';

describe('ScrappedLogsComponent', () => {
  let component: ScrappedLogsComponent;
  let fixture: ComponentFixture<ScrappedLogsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScrappedLogsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScrappedLogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
