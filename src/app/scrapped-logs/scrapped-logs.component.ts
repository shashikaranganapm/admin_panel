import { Component, OnInit } from '@angular/core';
import {DataService} from '../services/data.service';
import {ExportAsService, ExportAsConfig} from 'ngx-export-as';

import {log} from 'util';

@Component ({
  selector: 'app-icons',
  templateUrl: './scrapped-logs.component.html',
  styleUrls: ['./scrapped-logs.component.css']
})
export class ScrappedLogsComponent implements OnInit {
  config: ExportAsConfig = {
    type: 'pdf',
    elementId: 'logsTable',
  };

  isVisible = false;
  scrappedList=[];
  scrappedLogs;
    nodataVisible = false;
  constructor(private dataService: DataService,private exportAsService: ExportAsService) {}

  ngOnInit() {
    this.getScrappedSite();
  }

  // get scrapping sites list
  getScrappedSite(){
    this.dataService.getScrappedSites().subscribe(result =>{
      this.scrappedList = result;
    })

  }

  // get all scrapping site logs
  scrappingLogs(url){
    this.dataService.getScrappingSitesLog(url).subscribe(result=>{
      this.scrappedLogs=result;
      if (result.message == null){
        this.nodataVisible = true;
      }
      this.isVisible =!this.isVisible;
    });

  }
  // download pdf
  exportAs(type) {
    this.config.type = type;
    this.exportAsService.save(this.config, 'scrappedLogs');
    // this.isVisible =false;
  }

}
