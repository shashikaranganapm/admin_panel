import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebsitesAndDeviceListComponent } from './websites and device-list.component';

describe('WebsitesAndDeviceListComponent', () => {
  let component: WebsitesAndDeviceListComponent;
  let fixture: ComponentFixture<WebsitesAndDeviceListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebsitesAndDeviceListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebsitesAndDeviceListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
