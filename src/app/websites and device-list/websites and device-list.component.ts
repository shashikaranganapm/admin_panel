import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { filter } from 'rxjs-compat/operator/filter';
import { query } from '@angular/animations';
import { enableBindings } from '@angular/core/src/render3';
import { of, } from 'rxjs';
import { delay } from 'rxjs/operators';
import { until } from 'selenium-webdriver';
import elementIsDisabled = until.elementIsDisabled;

declare var $: any;

@Component({
    selector: 'app-table-list',
    templateUrl: './websites and device-list.component.html',
    styleUrls: ['./websites and device-list.component.css']
})

export class WebsitesAndDeviceListComponent implements OnInit {

    countVisible = true;
    filterTotal;
    filterDeviceOffset;
    allProductsTotal;
    allDeviceOffset;
    hideAllRecordLabel = true;
    hideFilterRecordlabel = false;
    tableIsEmpty = false;
    tempTotal;
    previousBtnVisible = false;
    nexBtnVisible = true;
    PageNo = 1;
    nextBtn = true;
    filter = {
        offset: 10,
        sort: -1,
        sortBy: 'capacity',
        limit: 10
    };
    filterDeviceOffsetBigin=1;
    isDisable = false;
    model: any = {
        siteName: '',
        brandName: '',
        modelName: '',
        networkName: '',
        capacity: '',
        condition: '',
        priceFrom: '',
        priceTo: ''
    };

    filterButton;

    products = [];
    loadModelNames = [];
    modelNamesBackup = [];
    loadSites = [];
    loadBrands = [];
    loadNetwork = [];
    loadCapacities = [];
    loadConditions = [];

    constructor(private dataService: DataService) {
    }

    ngOnInit() {
        this.fetchAllProducts(this.filter);
        this.loadModelName();
        this.loadSiteName();
        this.loadNetworkName();
        this.loadBrandsName();
        this.loadCondition();
        this.loadCapacity();
    }


    showNotification(from, align, mtype, message) {
        const type = [mtype];

        const color = 'red';

        $.notify({
            icon: 'notifications',
            message: '<b style="color: black">' + message + '<b>'

        }, {
                type: type[color],
                timer: 2000,
                placement: {
                    from: from,
                    align: align
                },
            });
    }

    // get all websites device details
    fetchAllProducts(filter) {
        this.dataService.getProducts(filter).subscribe(result => {
            console.log(result);
            this.products = result.docs;
            this.allProductsTotal = result.total;
            this.allDeviceOffset = result.offset;
            this.hideFilterRecordlabel = false;
            this.hideAllRecordLabel = true;
            // this.isVisible = false;
            if(this. allDeviceOffset==10){
                this.filterDeviceOffsetBigin=1;
                this.previousBtnVisible =false;
            } else{
                this.filterDeviceOffsetBigin=this.allDeviceOffset-10;
                this.previousBtnVisible =true;
            }
        });
    }

    // filter main scrapping data
    filterData(filter) {
        this.tableIsEmpty = false;
        this.dataService.filterScrappingData(filter, this.model).subscribe(result => {
            console.log(result);
            this.products = result.docs;
            this.filterTotal = result.total;
            this.filterDeviceOffset = result.offset;
            this.hideFilterRecordlabel = true;
            this.hideAllRecordLabel = false;
            if (result.message != null) {
                this.showNotification('bottom', 'right', 'delete', result.message);
                this.tableIsEmpty = true;
                this.countVisible = false;
            } else {
                this.products = result.docs;
                this.filter.offset;
                this.countVisible = true;

            }
            if(this. filterDeviceOffset==10){
                this.filterDeviceOffsetBigin=1;
                this.previousBtnVisible =false;
            } else{
                this.filterDeviceOffsetBigin=this.filterDeviceOffset-10;
                this.previousBtnVisible =true;
            }
        });
    }
    // price input validation
    checkValues(event: number) {
        this.model.priceTo = event;
        if (!(Number(this.model.priceFrom) < event)) {
            this.showNotification('top', 'right', 'red', 'PriceTo should be greater than PriceFrom value');
        }
    }

    // clear all text field
    clear() {
        this.fetchAllProducts(filter);
        this.model.siteName = '';
        this.model.brandName = '';
        this.model.modelName = '';
        this.model.networkName = '';
        this.model.capacity = '';
        this.model.condition = '';
        this.model.priceFrom = '';
        this.model.priceTo = '';
        this.tableIsEmpty = false;
        this.loadModelNames=this.modelNamesBackup;
        this.countVisible = true;
        this.ngOnInit();


    }

    // load all modelName to dropdown
    loadModelName() {
        this.dataService.getAllModleName().subscribe(result => {
            this.loadModelNames = result;
            this.modelNamesBackup = result;
        });
    }
    // load all siteName to dropdown
    loadSiteName() {
        this.dataService.getAllSites().subscribe(result => {
            this.loadSites = result;
        });
    }
    // load all networkName to dropdown
    loadNetworkName() {
        this.dataService.getAllNetwork().subscribe(result => {
            this.loadNetwork = result;
        });
    }

    // load all brandName to dropdown
    loadBrandsName() {
        this.dataService.getAllBrands().subscribe(result => {
            this.loadBrands = result;
        });
    }

    // load all conditions to dropdown
    loadCondition() {
        this.dataService.getAllConditons().subscribe(result => {
            this.loadConditions = result;
        })
    }

    // load all capacities to dropdown
    loadCapacity() {
        this.dataService.getAllCapacity().subscribe(result => {
            this.loadCapacities = result;
        });
    }


    // next pagination
    nextPage() {
        this.filter.offset += 10;
        this.allDeviceOffset +=10;
        if (this.filter.offset + 10 > this.allProductsTotal) {
            this.nexBtnVisible = false;

        }

        if (this.model.type !== '' && this.model.select !== '') {
            this.filterDeviceOffset +=10;
            this.allDeviceOffset +=10;
            this.previousBtnVisible =true;
            this.filterData(this.filter);


        } else {
            this.allDeviceOffset +=10;
            this.fetchAllProducts(this.filter);
            this.filter.offset += 10;
        }
        this.PageNo +=1;
    }

//     this.filter.offset += 10;
//     this.uniqueDeviceOffset +=10;
//
//     if (this.filter.offset+10 > this.uniqueDeviceTotal){
//     this.nexBtnVisible = false;
//     this.pageNumber +=1;
// }
// this.fetchDevices(this.filter);
// this.filter.offset += 10;

    // previous Pagination
    previousPage() {
        this.nexBtnVisible = true;
        this.PageNo -=1;
        this.filter.offset <= 0 ? this.filter.offset = 0 : this.filter.offset -= 20;
        this.allDeviceOffset <= 0 ? this.allDeviceOffset =0 : this.allDeviceOffset -=10;
        if (this.model.type !== '' && this.model.select !== '') {
            // this.allDeviceOffset -=10;
            this.filterData(this.filter);

        } else {
            // this.filterDeviceOffset -=10;
            this.fetchAllProducts(this.filter);
        }
    }

    // detail list ascending
    ascending() {
        this.filter.sort = -1;
        if (this.model.type !== '' && this.model.select !== '') {
            this.filterData(this.filter);

        } else {
            this.fetchAllProducts(this.filter);
        }

    }

    // detail list descending
    descending() {
        this.filter.sort = 1;
        if (this.model.type !== '' && this.model.select !== '') {
            this.filterData(this.filter);

        } else {
            this.fetchAllProducts(this.filter);
        }

    }

    // to select brand
    selectBrand(brand) {
        let filterdModles = [];
        for (let modleName of this.modelNamesBackup) {
            if (modleName.includes(brand)) {
                filterdModles.push(modleName);
            }
        }
        this.loadModelNames = filterdModles;
    }
}
