import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import { NgHttpLoaderModule } from 'ng-http-loader';
import { BrowserModule } from '@angular/platform-browser';
import {AppRoutingModule} from './app.routing';
import {ComponentsModule} from './components/components.module';

import {AppComponent} from './app.component';

import {DashboardComponent} from './dashboard/dashboard.component';
import {UserProfileComponent} from './user-profile/user-profile.component';
import {WebsitesAndDeviceListComponent} from './websites and device-list/websites and device-list.component';
import {UniqueDeviceComponent} from './unique-device/unique-device.component';
import {ScrappedLogsComponent} from './scrapped-logs/scrapped-logs.component';
import {MapsComponent} from './maps/maps.component';
import {NotificationsComponent} from './notifications/notifications.component';
import {SettingsComponent} from './settings/settings.component';
import { MatProgressSpinnerModule } from '@angular/material';
import { NgxSpinnerModule } from 'ngx-spinner';
import {AgmCoreModule} from '@agm/core';
import {AdminLayoutComponent} from './layouts/admin-layout/admin-layout.component';
import {LoginComponent} from './login/login.component';
import {MatInputModule, MatDividerModule, MatButtonModule, MatCardModule, MatIconModule} from '@angular/material';
import {CookieService} from 'ngx-cookie-service';
import {JwtModule} from '@auth0/angular-jwt';
import {MatSelectModule} from '@angular/material/select';
export function tokenGetter() {
    return localStorage.getItem('token');
}

import {MatDialogModule} from '@angular/material/dialog';

@NgModule({
    imports: [
        BrowserAnimationsModule,
        FormsModule,
        HttpModule,
        ComponentsModule,
        RouterModule,
        HttpClientModule,
        BrowserModule,
        NgHttpLoaderModule.forRoot(),
        AppRoutingModule,
        MatSelectModule,
        MatDividerModule,
        MatCardModule,
        MatIconModule,
        NgxSpinnerModule,
        MatDialogModule,
        MatProgressSpinnerModule,
        MatButtonModule,
        JwtModule.forRoot({
            config: {
                tokenGetter: tokenGetter,
                whitelistedDomains: ['localhost:3000']
            }
        }),
        MatInputModule,
        AgmCoreModule.forRoot({
            apiKey: 'YOUR_GOOGLE_MAPS_API_KEY'
        })
    ],
    declarations: [
        AppComponent,
        AdminLayoutComponent,
        LoginComponent,
        // DashboardComponent,
        // UserProfileComponent,
        // WebsitesAndDeviceListComponent,
        // UniqueDeviceComponent,
        // ScrappedLogsComponent,
        // MapsComponent,
        // NotificationsComponent,
        // SettingsComponent,
    ],
    providers: [CookieService],
    bootstrap: [AppComponent],
    schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA]

})
export class AppModule {
}
