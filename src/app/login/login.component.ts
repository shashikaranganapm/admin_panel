import {Component, OnInit, Input} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';

import {DataService} from '../services/data.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    showWarn = false;

    @Input() user = {email: '', password: ''};

    token = '';

    constructor(private dataService: DataService, private route: ActivatedRoute, private router: Router, private cookieService: CookieService) {
        this.dataService.login = false;
    }

    ngOnInit() {
        if (this.cookieService.get('token') !== '') {
            this.router.navigate(['dashboard']);
        }
    }

    // user login
    loginUser() {
        this.showWarn = false;
        this.dataService.loginUser(this.user).subscribe((result) => {
            // console.log(result);
            if (result && result.auth) {
                this.cookieService.set('token', result.token, 10);
                this.token = this.cookieService.get('token')
                localStorage.setItem('token', this.token);
                this.dataService.login = true;
                this.router.navigate(['dashboard']);
            } else {
                this.showWarn = true;
                setTimeout(() => {
                    this.showWarn = false;
                }, 5000);
            }
        }, (err) => {
            console.log(err);
        });
    }

}
