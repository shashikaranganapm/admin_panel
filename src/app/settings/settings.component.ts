import {Component, OnInit} from '@angular/core';
import {DataService} from 'app/services/data.service';

declare var $: any;

@Component({
    selector: 'app-upgrade',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

    model: any = {
        newPassword: '',
        confirmNewPassword: ''
    };

    constructor(private dataService: DataService) {
    }


    ngOnInit() {
    }

    // clear all text field
    clearTextField() {
        this.model.newPassword = '';
        this.model.confirmNewPassword = '';
    }

    // alert
    showNotification(from, align, mtype, message) {
        const type = [mtype];

        const color = Math.floor((Math.random() * 4) + 1);

        $.notify({
            icon: 'notifications',
            message: '<b style="color: black">' + message + '<b>'

        }, {
            type: type[color],
            timer: 3500,
            placement: {
                from: from,
                align: align
            },
        });
    }

    // Update user password
    changePassword() {
        this.dataService.changePassword(this.model).subscribe((res) => {
            if (res.success) {
                this.clearTextField();
                this.showNotification('bottom', 'right','save','Your Passsword Changed Success !');
            } else {
                this.showNotification('bottom', 'right','danger','Passsword should contain 8 characters and no more than 8 characters !');
            }

        });

    }

}
