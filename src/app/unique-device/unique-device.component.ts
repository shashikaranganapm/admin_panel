import {Component, OnInit} from '@angular/core';
import {DataService} from '../services/data.service';
import {timeout} from 'rxjs-compat/operator/timeout';
import {timer} from 'rxjs';


declare var $: any;

@Component({
    selector: 'app-typography',
    templateUrl: './unique-device.component.html',
    styleUrls: ['./unique-device.component.css']
})
export class UniqueDeviceComponent implements OnInit {
    uniqueDeviceTotal;
    uniqueDeviceOffset;
    uniqueDeviceOffsetBigin=1;
    nexBtnVisible = true;
    previousBtnVisible = false;
    pageNumber = 1;
    filter = {
        offset: 10,
        sort: -1,
        sortBy: 'slugName',
        limit: 10
    }
    devices = [];
    model: any = {
        deviceName: '',
    };

    constructor(private dataService: DataService) {
    }

    ngOnInit() {
        this.fetchDevices(this.filter);
    }

    // get all unique devices
    fetchDevices(filter) {
        this.dataService.getUniqueDevices(filter).subscribe(result => {
            console.log(result);
            this.devices = result.docs;
            this.uniqueDeviceTotal = result.total;
            this.uniqueDeviceOffset = result.offset;
            if(this.uniqueDeviceOffset==10){ 
                this.uniqueDeviceOffsetBigin=1;
                this.previousBtnVisible =false;
            } else{
                this.uniqueDeviceOffsetBigin=this.uniqueDeviceOffset-10;
                this.previousBtnVisible =true;
            }
        });

    }

    // alert
    showNotification(from, align, mtype, message) {
        const type = [mtype];

        const color = 'red';

        $.notify({
            icon: 'notifications',
            message: '<b style="color: black">' + message + '<b>'

        }, {
            type: type[color],
            timer: 2500,
            placement: {
                from: from,
                align: align
            },
        });
    }


    // clear all text field
    clearTxtFeald(){
        this.model.deviceName='';
    }

    // add unique device to main db
    addDevice() {
        this.dataService.addDevice(this.model).subscribe((result) => {
            // this.tempTotal = result.
            if(result !== undefined){
                if (result.success) {
                    this.showNotification('bottom', 'right', 'save', result.message);
                    this.fetchDevices(this.filter);
                    this.clearTxtFeald();
                } else {
                    this.showNotification('bottom', 'right', 'red', result.message);
                }
           }else{
            this.showNotification('bottom', 'right', 'red', 'Entered name is not valid');
           }
        });
    }


    // next pagination
    nextPage(){
        this.filter.offset += 10;
        this.uniqueDeviceOffset +=10;

        if (this.filter.offset+10 > this.uniqueDeviceTotal){
            this.nexBtnVisible = false;
            this.pageNumber +=1;
        }
        this.fetchDevices(this.filter);
        this.filter.offset += 10;

    }

    // previous pagination
    previousPage(){
        this.nexBtnVisible = true;
        this.filter.offset <= 0 ? this.filter.offset = 0 : this.filter.offset -= 20;
        this.uniqueDeviceOffset <=0? this.uniqueDeviceOffset = 0: this.uniqueDeviceOffset -=10;
        this.pageNumber -=1;
        this.fetchDevices(this.filter);

    }

    // device list ascending
    ascending(){
        this.filter.sort = -1;
        this.fetchDevices(this.filter);
    }

    // device list descending
    descending(){
        this.filter.sort = 1;
        this.fetchDevices(this.filter);
    }
}
