import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UniqueDeviceComponent } from './unique-device.component';

describe('UniqueDeviceComponent', () => {
  let component: UniqueDeviceComponent;
  let fixture: ComponentFixture<UniqueDeviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UniqueDeviceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UniqueDeviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
