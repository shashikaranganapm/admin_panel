import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { WebsitesAndDeviceListComponent } from '../../websites and device-list/websites and device-list.component';
import { UniqueDeviceComponent } from '../../unique-device/unique-device.component';
import { ScrappedLogsComponent } from '../../scrapped-logs/scrapped-logs.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { SettingsComponent } from '../../settings/settings.component';

import {
  MatButtonModule,
  MatInputModule,
  MatRippleModule,
  MatFormFieldModule,
  MatTooltipModule,
  MatSelectModule
} from '@angular/material';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
  ],
  declarations: [
    DashboardComponent,
    UserProfileComponent,
    WebsitesAndDeviceListComponent,
    UniqueDeviceComponent,
    ScrappedLogsComponent,
    MapsComponent,
    NotificationsComponent,
    SettingsComponent,
  ]
})

export class AdminLayoutModule {}
